sudo apt-get install python3-pip
sudo python3 -m pip install pipenv
pipenv install
if [[ $(lsb_release -rs) == "18.04" ]]; then
    sudo apt update
    sudo apt install -y mongodb
else
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
    echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org --allow-unauthenticated
    sudo systemctl start mongod
    sudo systemctl enable mongod
fi
pipenv install flask-mongoengine
cd frontend
sudo apt-get install curl
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn
sudo apt-get install npm
sudo npm install -g n
sudo n latest
yarn install
yarn build
if [[ $(lsb_release -rs) == "18.04" ]]; then
    sudo apt-get update -y
    sudo apt-get install -y mediainfo
else
    wget https://mediaarea.net/download/binary/libzen0/0.4.37/libzen0v5_0.4.37-1_amd64.xUbuntu_16.04.deb
    wget https://mediaarea.net/download/binary/libmediainfo0/19.09/libmediainfo0v5_19.09-1_amd64.xUbuntu_16.04.deb
    wget https://mediaarea.net/download/binary/mediainfo/19.09/mediainfo_19.09-1_amd64.xUbuntu_16.04.deb
    sudo dpkg --install libzen0v5_0.4.37-1_amd64.xUbuntu_16.04.deb
    sudo apt-get update
    sudo apt-get install libmms0
    sudo dpkg --install libmediainfo0v5_19.09-1_amd64.xUbuntu_16.04.deb
    sudo dpkg --install mediainfo_19.09-1_amd64.xUbuntu_16.04.deb
fi
pip install coverage

echo -e "\nIP ADDRESS SETUP: \n
-Go to: frontend/src/views/ \n
-Open Search.vue in editor \n
-Change ip address in line 95 to ip address \nof the machine that this is being depoloyed to
        indexer_port: 'http://192.168.238.93:5000',\n --> indexer_port: 'http://<ip address of machine>:5000', \n
-Save changes
-Open Admin.vue in editor
-Change ip address in line 51 to ip address \nof the machine that this is being depoloyed to
        indexer_port: 'http://192.168.238.93:3000',\n --> indexer_port: 'http://<ip address of machine>:3000', \n
-Save changes \n
-Go to /media-indexer\n"

read -n 1 -s -r -p "Press any key to continue"
echo -e "\nThank you for installing Gofer media indexer!"
