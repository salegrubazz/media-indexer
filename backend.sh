#!/bin/sh
cd backend
intexit() {
    # Kill all subprocesses (all processes in the current process group)
    kill -HUP -$$
}

hupexit() {
    # HUP'd (probably by intexit)
    echo
    echo "Interrupted"
    exit
}

trap hupexit HUP
trap intexit INT

pipenv run python patroller.py &
pipenv run python indexer.py &
bash metagen_script.sh &

wait
