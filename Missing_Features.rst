What is this?
-------------
This document lists down the features missing in Gofer media indexer. 

Modules:

* Login and authentication system. Admin module shouldn't be available to unauthenticated users.
* Port protection against undefined and unauthenticated requests.
* Global port configuration for Vue.js modules (frontend).
* Log and action history system.
* Cloud database.
* Segmentation implementation: Current version can be setup with all modules on different computers because of microservice architecture, but with few modifications. This can be further implemented in Admin module, where an option for choosing a port can be implemented. This would basically let the user implement Indexer.py module on several different machines, start them all on different ports and use any of them for data saving and searching.
    * Step further in segmentation: Implementation of multiple running instances of server on different machines, which can be used for faster computation because there will be several parallel instances working simultaneously.
* Port for Windows and IOS.

Admin module:

* Real time progress visible to user.
* Clearing the database and deleting certain data from the database remotely through this module.

Search module:

* Extracted data about found files (currently only file path(s) are returned to the user).
* Statistics for queried data.

Metagen:

* Implementing different indexing and parsing units (currently only MediaInfo implemented).
    * This can be done even with parsing units that search for certain patterns, keywords or data in textual files, which can effectively turn Gofer into a search engine for textual database (Data curation).

TESTS:

* Integration tests that incapsulate whole services are not finished.
