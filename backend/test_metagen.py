import unittest
import os
from unittest import mock
from unittest.mock import patch
import metagen
from metagen import app
from requests.exceptions import HTTPError

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
f_name = ROOT_DIR + '/test_files/test_file.txt'


class TestRequestsCall(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setupClass')

    @classmethod
    def tearDownClass(cls):
        print('teardownClass')

    def setUp(self):
        app.config['TESTING'] = True
        self.client = app.test_client()
        self.test_fname = f_name
        print('setUp')

    def tearDown(self):
        print('tearDown\n')

    def _mock_response(
            self,
            status=200,
            content="CONTENT",
            json_data=None,
            text=None,
            raise_for_status=None):
        """
        since we typically test a bunch of different
        requests calls for a service, we are going to do
        a lot of mock responses, so its usually a good idea
        to have a helper function that builds these things
        """
        mock_resp = mock.Mock()
        # mock raise_for_status call w/optional error
        mock_resp.raise_for_status = mock.Mock()
        if raise_for_status:
            mock_resp.raise_for_status.side_effect = raise_for_status
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content
        mock_resp.text = text
        # add json data if provided
        if json_data:
            mock_resp.json = mock.Mock(
                return_value=json_data
            )
        return mock_resp

    @mock.patch('requests.post')
    def test_call_mediainfo(self, mock_post):
        test_fname = self.test_fname
        mock_resp = self._mock_response(text=test_fname)

        mock_post.return_value = mock_resp
        result = metagen.call_mediainfo(test_fname)
        print(result)
        test_file_parsed = test_fname

        self.assertEqual(result, test_file_parsed)

    @mock.patch('requests.post')
    def test_call_mediainfo_failed_query(self, mock_post):
        test_fname = self.test_fname
        mock_resp = self._mock_response(status=500, raise_for_status=HTTPError(
            "Gofer is down"))

        mock_post.return_value = mock_resp
        self.assertRaises(HTTPError, metagen.call_mediainfo, test_fname)

    def test_extract_metadata(self):
        with patch('metagen.call_mediainfo') as mocked_call:
            mocked_call.return_value = self.test_fname

            response = self.client.post(
                '/', json={'path': self.test_fname, 'parser': 'MediaInfo'},
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, self.test_fname)

    def test_extract_metadata_error(self):
        with patch('metagen.call_mediainfo') as mocked_call:
            mocked_call.return_value = self.test_fname

            response = self.client.post(
                '/', json={'path': self.test_fname, 'parser': 'Randomness'},
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result,  '\nERROR: Parser not implemented.\n')


if __name__ == '__main__':
    unittest.main()
