#!/bin/sh

pipenv run coverage run -m unittest test_patroller.py 
pipenv run coverage report -m patroller.py
pipenv run coverage run -m unittest test_metagen.py 
pipenv run coverage report -m metagen.py
pipenv run coverage run -m unittest test_indexer.py 
pipenv run coverage report -m indexer.py

wait
