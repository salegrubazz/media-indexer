from flask import Flask, request        # flask framework and http methods
from flask_cors import CORS     # Additional http methods
from flask_mongoengine import MongoEngine       # flask methods for mongodb
from mongoengine.errors import NotUniqueError       # mongodb error handle
from models import File     # mongodb objects architecture
import config      # config module with all module ports
import ast      # used to create dictinoary from unicode
import collections      # for sorting dictionaries


db = MongoEngine()      # initilazing mongo data base
app = Flask(__name__)

# this configures mongo data base
app.config['MONGODB_SETTINGS'] = {
    'host': 'localhost',
    'port': 27017,
    'db': 'meta_data',
}
db.init_app(app)
CORS(app)


# This is a function that stores files in database.
def add_file(data):
    # extracts file_path from meta data extracted by mediaInfo
    file_path = data['tracks'][0]['complete_name']
    # extracts folder_path from meta data extracted by mediaInfo
    folder_path = data['tracks'][0]['folder_name']
    # extracts basic meta data from meta data extracted by mediaInfo
    file_data = data['tracks'][0]
    # extracts stream meta data from meta data extracted by mediaInfo
    stream_data = data['tracks'][1:]
    # list that contains all keys in file_data and stream_data
    data_key_list = []
    # this will populate data_key_list
    for key, value in file_data.items():
        data_key_list.append(key)
    for item in stream_data:
        for key, value in item.items():
            # utc_ files are exluded due to their number and because they--
            # are not important. Client request
            if 'utc_' not in key:
                data_key_list.append(key)
    # creates database object
    file = File(file_path=file_path,
                folder_path=folder_path, data_key_list=data_key_list,
                file_data=file_data, stream_data=stream_data)
    # tries to save object, if not unique it will delete old copy from the--
    # database and add new copy to the database
    try:
        file.save()
    except NotUniqueError as e:
        not_unique_file = File.objects.get(file_path=file_path)
        not_unique_file.delete()
        if e is not None:
            file.save()
        updated_file_path = 'Updated: '+file_path
        return updated_file_path
    # returns file path of added file
    return 'Added to the database: '+file_path


# This is a function that returns all keys stored in database.
def all_keys():
    # meta_data now contains all distinct keys present in DB
    meta_data = File.objects.distinct('data_key_list')
    # this turns meta_data to a list
    meta_data = list(meta_data)
    return meta_data


# This is a function that queries list of key:value pairs.
def query_values_list(list_of_keys, list_of_values):
    # this counter is used for iterating through lists of keys and values
    counter = 0
    # query_list contains file paths of first query matches
    query_list = query_value(list_of_keys[counter], list_of_values[counter])
    # here every next query will be checked if there are any matching file--
    # paths. At the end of this for query_list will contain all matching--
    # files (if any) of all sent queries
    for item in list_of_keys:
        tmp_list = query_value(item, list_of_values[counter])
        counter = counter+1
        query_list = list(set(query_list) & set(tmp_list))
    return query_list


# This is a function that queries all values for a certain key.
def all_value_query(key):
    return_list = []
    # searches for queried key in stream data and adds its values--
    # to return list (if any are found)
    full_key = 'stream_data.'+key
    meta_data = File.objects.distinct(full_key)
    return_list.append(meta_data)
    # searches for queried key in file data and adds its values--
    # to return list (if any are found)
    full_key = 'file_data.'+key
    meta_data = File.objects.distinct(full_key)
    return_list.append(meta_data)
    # flat_list is used to flatten return_list
    flat_list = []
    # here return_list is flattened
    for sublist in return_list:
        for item in sublist:
            flat_list.append(item)
    # list of all values under a certain key is returned
    return flat_list


# This is a function that queries a single key:value pair.
def query_value(key, value):
    # searches for key in stream data
    key_stream = 'stream_data.'+key
    # this is a raw query into DB for nested value
    query = File.objects(__raw__={key_stream: value})
    # extracts file_paths from queried files
    files = query.only('file_path').all()
    tmp_file_list = []
    for obj in files:
        tmp_file_list.append(obj.file_path)
    # searches for key in file data
    key = 'file_data.'+key
    # this is a raw query into DB for nested value
    query = File.objects(__raw__={key: value})
    # extracts file_paths from queried files
    files = query.only('file_path').all()
    for obj in files:
        tmp_file_list.append(obj.file_path)
    # returns a list of file_paths leading to files of chosen query
    return tmp_file_list


# This is a function that returns all file paths in database.
def get_all_files(only_paths):
    files = File.objects
    # query for all file paths in database
    files = files.only('file_path').all()
    tmp_file_list = []
    for obj in files:
        tmp_file_list.append(obj.file_path)
    # returns all file paths in database
    return tmp_file_list


# This is a function that returns all objects with same folder path.
def get_all_folder_files(folder_path):
    # filters through folder_path args of DB objects for queried folder path
    files = File.objects.filter(folder_path=folder_path)
    # extracts file paths of all objects with queried folder_path
    files = files.only('file_path').all()
    tmp_file_list = []
    for obj in files:
        tmp_file_list.append(obj.file_path)
    # returns file paths of all objects with queried folder_path
    return tmp_file_list


# This is a function that returns all subfolders of a chosen folder.
def map_folder_paths(folder_path):
    # filters all objects which have queried folder_path or which--
    # folder_paths are subfolders to the queried one
    document = File.objects(
        folder_path__startswith=folder_path).all()
    tmp_file_list = []
    for obj in document:
        tmp_file_list.append(obj.folder_path)
    file_list = list(dict.fromkeys(tmp_file_list))
    # returns file path list of requested objects
    return file_list


# This is a function that returns single file through its file path.
def get_single_file(file_path):
    # query is based on file_path, which is unique
    return File.objects.get(file_path=file_path).asdict()


"""
This route has three different methods.
Get method handles multiple possible parameters.
Possible parameters:
'folder_path': Returns all files with the same folder path,
'all_value_query': Returns all values under a single key,
'all_keys': Returns all keys in the data_base,
'folder_map_path': Returns all subfolders of a chosen folder,
'file_path': Returns filepath of a single file,
'only_paths': Returns all filespaths in database.
If get method is called and it doesn't contain any of these parameters,
then it is a query. This will take 'key' and 'value' lists from
parameters and will create a query set for database. After that the
return value will be all filepaths of files that have all sent queries.
Post method is used to add file meta data to the database.
Delete method is used to remove file meta data from the database.
"""
@app.route('/', methods=['GET', 'POST', 'DELETE'])
def all_files():
    if request.method == 'GET':
        # checks for 'folder_path' argument
        folder_path = request.args.get('folder_path', None)
        if folder_path is not None:
            files = get_all_folder_files(folder_path)
            # returns all files with the same folder path
            return {
                'files': files
            }

        # checks for 'all_value_query' argument
        client_request = request.args.get('all_value_query', None)
        if client_request is not None:
            files = all_value_query(client_request)
            # returns all values under a single key
            return {
                'files': files
            }

        # checks for 'all_keys' argument
        client_request = request.args.get('all_keys', None)
        if client_request is not None:
            files = all_keys()
            # returns all keys in the data_base
            return {
                'files': files
            }

        # checks for 'folder_map_path' argument
        folder_map_path = request.args.get('folder_map_path', None)
        if folder_map_path is not None:
            files = map_folder_paths(folder_map_path)
            # returns all subfolders of a chosen folder
            return {
                'files': files
            }

        # checks for 'file_path' argument
        file_path = request.args.get('file_path', None)
        if file_path is not None:
            # returns filepath of a single file
            return get_single_file(file_path)

        # checks for 'only_paths' argument
        only_paths = request.args.get('only_paths', None)
        if only_paths is not None:
            files = get_all_files(only_paths)
            # returns all filespaths in database
            return {
                'files': files
            }

        # if no defined arguments are given, the the request is a query
        # this is a list of query keys
        key_list = []  # request.values.getlist('key', None)
        # this is a list of query values
        value_list = []  # request.values.getlist('value', None)

        # this handles javascript array
        # first it is transformed to dict
        request_dict = request.values.to_dict()
        # request data must be sorted after being received from vue.js
        ordered_request = collections.OrderedDict(sorted(request_dict.items()))
        # then a list of values is created. Values are key:value pairs--
        # but are saved as strings
        request_dict = list(ordered_request.values())

        # here every item from the list is turned into a single key:value--
        # pair dict with literal_eval. After that values and keys are saved--
        # in value and key lists.
        for item in request_dict:
            key_val_pair = ast.literal_eval(item)
            if key_val_pair.get('value') is not None:
                value_list.append(key_val_pair.get('value'))
            else:
                key_list.append(key_val_pair.get('key'))
        # this calls a function that queries list of key:value pairs and--
        # returns file paths of files that are in all queries
        files = query_values_list(key_list, value_list)

        return {
            'files': files
        }

    elif request.method == 'POST':
        # this will add a file to the DB
        return add_file(request.json)

    elif request.method == 'DELETE':
        # this will delete a single object from the database
        # file_path is sent as an argument here
        # TODO: check needed for safety reasons
        file_path = request.args.get('file_path', None)
        data = request.json
        path = data['file_path']
        tmp = File.objects.get(file_path=path)
        tmp.delete()

        return 'Deleted (does not exist in memory): '+path


if __name__ == '__main__':
    # 0.0.0.0 host is local IP in flask
    app.run(host='0.0.0.0', port=config.indexer_socket)
