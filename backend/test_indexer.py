import unittest
import os
from unittest import mock
from unittest.mock import patch
import indexer
from indexer import app
import test_json
from models import File
from mongoengine import connect, disconnect

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
f_name = ROOT_DIR + '/test_files/test_file.txt'
folder_path = ROOT_DIR + '/test_files'


class TestPerson(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        disconnect()
        connect('mongoenginetest', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        data = test_json.test
        indexer.add_file(data)
        print('setUp')

    def tearDown(self):
        tmp = File.objects.get(file_path=f_name)
        tmp.delete()
        print('tearDown\n')

    def _mock_response(
            self,
            status=200,
            content="CONTENT",
            json_data=None,
            text=None,
            raise_for_status=None):
        """
        since we typically test a bunch of different
        requests calls for a service, we are going to do
        a lot of mock responses, so its usually a good idea
        to have a helper function that builds these things
        """
        mock_resp = mock.Mock()
        # mock raise_for_status call w/optional error
        mock_resp.raise_for_status = mock.Mock()
        if raise_for_status:
            mock_resp.raise_for_status.side_effect = raise_for_status
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content
        mock_resp.text = text
        # add json data if provided
        if json_data:
            mock_resp.json = mock.Mock(
                return_value=json_data
            )
        return mock_resp

    def test_add_file(self):
        data = test_json.test
        response = indexer.add_file(data)
        self.assertEqual(
            response,  'Updated: /home/rtrk/BSCprojekat/media-indexer/backend/test_files/test_file.txt')

    def test_add_file_unique(self):
        tmp = File.objects.get(file_path=f_name)
        tmp.delete()
        data = test_json.test
        response = indexer.add_file(data)
        self.assertEqual(
            response,  'Added to the database: /home/rtrk/BSCprojekat/media-indexer/backend/test_files/test_file.txt')

    def test_all_keys(self):
        response = indexer.all_keys()
        self.assertListEqual(
            sorted(response), sorted(test_json.all_keys)
        )

    def test_query_values_list(self):
        with patch('indexer.query_value') as mocked_call:
            mocked_call.return_value = ['check']
            response = indexer.query_values_list(['arg1', 'arg2'], [0, 1])
            self.assertEqual(
                response, ['check']
            )

    def test_all_value_query(self):
        key = 'other_stream_size'
        list_for_assert = [' Byte0', '0.00 Byte', '0.000 Byte', '0.0 Byte']
        response = indexer.all_value_query(key)
        self.assertListEqual(
            sorted(response), sorted(list_for_assert)
        )

    def test_query_value(self):
        key = 'track_type'
        value = 'General'
        response = indexer.query_value(key, value)
        self.assertListEqual(
            sorted(response), [f_name]
        )
        key = 'test stream_data'
        value = [
            'mock stream_data1', 'mock stream_data2'
        ]
        response = indexer.query_value(key, value)
        self.assertListEqual(
            sorted(response), [f_name]
        )

    def test_get_all_files(self):
        only_paths = True
        response = indexer.get_all_files(only_paths)
        self.assertListEqual(
            sorted(response), [f_name]
        )

    def test_get_all_folder_files(self):
        response = indexer.get_all_folder_files(folder_path)
        self.assertListEqual(
            sorted(response), [f_name]
        )

    def test_map_folder_paths(self):
        response = indexer.map_folder_paths(ROOT_DIR)
        self.assertListEqual(
            sorted(response), [folder_path]
        )

    def test_get_single_file(self):
        response = indexer.get_single_file(f_name)
        self.assertListEqual(
            sorted(response), sorted(test_json.test_in_db)
        )


class TestRequestsCall(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        disconnect()
        connect('mongoenginetest', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        app.config['TESTING'] = True
        self.client = app.test_client()
        self.test_fname = f_name
        data = test_json.test
        indexer.add_file(data)
        print('setUp')

    def tearDown(self):
        tmp = File.objects.get(file_path=f_name)
        tmp.delete()
        print('tearDown\n')

    def _mock_response(
            self,
            status=200,
            content="CONTENT",
            json_data=None,
            text=None,
            raise_for_status=None):
        """
        since we typically test a bunch of different
        requests calls for a service, we are going to do
        a lot of mock responses, so its usually a good idea
        to have a helper function that builds these things
        """
        mock_resp = mock.Mock()
        # mock raise_for_status call w/optional error
        mock_resp.raise_for_status = mock.Mock()
        if raise_for_status:
            mock_resp.raise_for_status.side_effect = raise_for_status
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content
        mock_resp.text = text
        # add json data if provided
        if json_data:
            mock_resp.json = mock.Mock(
                return_value=json_data
            )
        return mock_resp

    def test_all_files(self):
        with patch('indexer.get_all_folder_files') as mocked_call:
            mocked_call.return_value = 'folder_path'
            response = self.client.get(
                '/?folder_path=True',
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":"folder_path"}\n')

        with patch('indexer.all_value_query') as mocked_call:
            mocked_call.return_value = 'all_value_query'
            response = self.client.get(
                '/?all_value_query=True',
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":"all_value_query"}\n')

        with patch('indexer.all_keys') as mocked_call:
            mocked_call.return_value = 'all_keys'
            response = self.client.get(
                '/?all_keys=True',
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":"all_keys"}\n')

        with patch('indexer.map_folder_paths') as mocked_call:
            mocked_call.return_value = 'folder_map_path'
            response = self.client.get(
                '/?folder_map_path=True',
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":"folder_map_path"}\n')

        with patch('indexer.get_single_file') as mocked_call:
            mocked_call.return_value = 'file_path'
            response = self.client.get(
                '/?file_path=True',
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, 'file_path')

        with patch('indexer.get_all_files') as mocked_call:
            mocked_call.return_value = 'only_paths'
            response = self.client.get(
                '/?only_paths=True',
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":"only_paths"}\n')

        with patch('indexer.query_values_list') as mocked_call:
            mocked_call.return_value = 'query_values'
            response = self.client.get(
                '/?{"key":"count"}={"value":"227"}'
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":"query_values"}\n')

        with patch('indexer.query_values_list') as mocked_call:
            mocked_call.return_value = 'query_values'
            response = self.client.get(
                '/?{"key":"value"}={"count":"227"}'
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":"query_values"}\n')

        with patch('indexer.add_file') as mocked_call:
            mocked_call.return_value = 'add_file'
            response = self.client.post(
                '/?{"key":"value"}={"count":"227"}'
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, 'add_file')

        response = self.client.delete(
            '/', json={'file_path': f_name}
        )
        result = response.data.decode('UTF-8')
        self.assertEqual(result, 'Deleted (does not exist in memory): '+f_name)
        data = test_json.test
        indexer.add_file(data)


if __name__ == '__main__':
    unittest.main()
