test = {'tracks': [{'other_stream_size': [
    '0.00 Byte', ' Byte0', '0.0 Byte', '0.00 Byte', '0.000 Byte'
], 'stream_identifier': '0', 'count_of_stream_of_this_kind': '1', 'other_kind_of_stream': [
    'General'
], 'file_last_modification_date': 'UTC 2020-01-13 09:31:58', 'track_type': 'General', 'file_last_modification_date__local': '2020-01-13 10:31:58', 'file_extension': 'txt', 'count': '331', 'other_file_size': [
    '0.00 Byte', ' Byte0', '0.0 Byte', '0.00 Byte', '0.000 Byte'
], 'complete_name': '/home/rtrk/BSCprojekat/media-indexer/backend/test_files/test_file.txt', 'file_size': 0, 'file_name': 'test_file', 'folder_name': '/home/rtrk/BSCprojekat/media-indexer/backend/test_files', 'file_name_extension': 'test_file.txt', 'stream_size': 0, 'kind_of_stream': 'General'},{
    'test stream_data': [
        'mock stream_data1', 'mock stream_data2'
    ]
}]}

test_in_db = {'file_path': '/home/rtrk/BSCprojekat/media-indexer/backend/test_files/test_file.txt', 'folder_path': '/home/rtrk/BSCprojekat/media-indexer/backend/test_files', 'data_key_list': ['stream_identifier', 'kind_of_stream', 'file_name_extension', 'other_file_size', 'file_size', 'count_of_stream_of_this_kind', 'complete_name', 'track_type', 'file_extension', 'stream_size', 'count', 'other_stream_size', 'file_last_modification_date', 'folder_name', 'other_kind_of_stream', 'file_name', 'file_last_modification_date__local', 'test stream_data'], 'stream_data': [{'test stream_data': ['mock stream_data1', 'mock stream_data2']}], 'file_data': {'stream_identifier': '0', 'kind_of_stream': 'General', 'file_name_extension': 'test_file.txt', 'other_file_size': ['0.00 Byte', ' Byte0', '0.0 Byte', '0.00 Byte', '0.000 Byte'], 'file_name': 'test_file', 'count_of_stream_of_this_kind': '1', 'complete_name': '/home/rtrk/BSCprojekat/media-indexer/backend/test_files/test_file.txt', 'track_type': 'General', 'file_extension': 'txt', 'stream_size': 0, 'count': '331', 'file_size': 0, 'file_last_modification_date': 'UTC 2020-01-13 09:31:58', 'folder_name': '/home/rtrk/BSCprojekat/media-indexer/backend/test_files', 'other_kind_of_stream': ['General'], 'file_last_modification_date__local': '2020-01-13 10:31:58', 'other_stream_size': ['0.00 Byte', ' Byte0', '0.0 Byte', '0.00 Byte', '0.000 Byte']}}

all_keys = ['track_type', 'kind_of_stream', 'file_last_modification_date__local', 'stream_size', 'file_size', 'complete_name', 'file_name_extension', 'file_name', 'file_last_modification_date', 'file_extension', 'other_file_size', 'count_of_stream_of_this_kind', 'other_stream_size', 'count', 'stream_identifier', 'folder_name', 'other_kind_of_stream', 'test stream_data']