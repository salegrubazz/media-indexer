import unittest
import patroller
import os
from patroller import app
from unittest import mock
from unittest.mock import patch
import requests

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
f_name = ROOT_DIR + '/test_files/test_file.txt'
folder_path = ROOT_DIR + '/test_files'


class TestPatroller(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setupClass')

    @classmethod
    def tearDownClass(cls):
        print('teardownClass')

    def setUp(self):
        app.config['TESTING'] = True
        self.client = app.test_client()
        self.test_fname = f_name
        print('setUp')

    def tearDown(self):
        print('tearDown\n')

    def _mock_response(
            self,
            status=200,
            content="CONTENT",
            json_data=None,
            text=None,
            raise_for_status=None):
        """
        since we typically test a bunch of different
        requests calls for a service, we are going to do
        a lot of mock responses, so its usually a good idea
        to have a helper function that builds these things
        """
        mock_resp = mock.Mock()
        # mock raise_for_status call w/optional error
        mock_resp.raise_for_status = mock.Mock()
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content
        mock_resp.text = text
        # add json data if provided
        if json_data:
            mock_resp.json = mock.Mock(
                return_value=json_data
            )
        return mock_resp

    @mock.patch('requests.get')
    def test_request_indexer_data(self, mock_get):
        test_fname = self.test_fname
        mock_resp = self._mock_response(json_data={'files': {'key': 'value'}})

        mock_get.return_value = mock_resp
        data = patroller.request_indexer_data(test_fname)
        self.assertEqual(data, {'key': 'value'})

    def test_return_parsers(self):
        response = patroller.return_parsers()
        self.assertEqual(response, patroller.config.parsers)

    @mock.patch('requests.post')
    def test_file_walk(self, mock_post):
        parser = 'MediaInfo'
        folder_path = '/home/rtrk/BSCprojekat/media-indexer/backend/test_files'
        mock_resp = self._mock_response()

        mock_post.return_value = mock_resp

        data = patroller.file_walk(folder_path, parser)
        self.assertEqual(data, {'files': [f_name]})

    @mock.patch('requests.post')
    def test_file_walk_connection_error(self, mock_post):
        parser = 'MediaInfo'
        mock_post.side_effect = requests.exceptions.ConnectionError()

        data = patroller.file_walk(folder_path, parser)
        f_error = 'Memory error on parser while parsing object. Could not parse this file: '+f_name
        self.assertEqual(data, {'files': [f_error]})

    @mock.patch('requests.post')
    def test_file_walk_update(self, mock_post):
        with patch('patroller.request_indexer_data') as mocked_call:
            mocked_call.return_value = []
            parser = 'MediaInfo'
            mock_resp = self._mock_response(text=f_name)

            mock_post.return_value = mock_resp

            data = patroller.file_walk_update(folder_path, parser)
            self.assertEqual(data, {'files': [f_name]})

    @mock.patch('requests.post')
    @mock.patch('requests.delete')
    def test_update_delete(self, mock_post, mock_delete):
        with patch('patroller.request_indexer_data') as mocked_call:
            with patch('patroller.obsolete_folder_delete') as mocked_call_2:
                mocked_call.return_value = [folder_path]
                mocked_call_2.return_value = ['deleted: check']
                parser = 'MediaInfo'
                mock_resp = self._mock_response()
                mock_resp_del = self._mock_response(text='deleted file: check')

                mock_post.return_value = mock_resp
                mock_delete.return_value = mock_resp_del

                data = patroller.file_walk_update(folder_path, parser)
                self.assertEqual(
                    data, {'files': ['deleted file: check', 'deleted: check', None]})

    @mock.patch('requests.post')
    def test_file_walk_update_connection_error(self, mock_post):
        with patch('patroller.request_indexer_data') as mocked_call:
            mocked_call.return_value = []
            parser = 'MediaInfo'
            mock_post.side_effect = requests.exceptions.ConnectionError()

            data = patroller.file_walk_update(folder_path, parser)
            f_error = 'Memory error on parser while parsing object. Could not parse this file: '+f_name
            self.assertEqual(data, {'files': [f_error]})

    @mock.patch('requests.delete')
    def test_obsolete_folder_delete(self, mock_delete):
        with patch('patroller.request_indexer_data') as mocked_call:
            mocked_call.return_value = [f_name]
            mock_resp = self._mock_response(text=f_name)

            mock_delete.return_value = mock_resp

            folder_path_list = []
            folder_path_list.append(folder_path)
            data = patroller.obsolete_folder_delete(folder_path_list)
            print(data)
            self.assertEqual(data, [f_name])

    def test_patrol(self):
        with patch('patroller.file_walk_update') as mocked_call_update:
            with patch('patroller.file_walk') as mocked_call:
                mocked_call_update.return_value = 'Update'
                mocked_call.return_value = 'non-Update'
                params = {'params': {'queryParser': {
                    'chosenParser': 'MediaInfo'}, 'data': {'path': folder_path}},
                }

                response = self.client.post(
                    '/file_walk', json=params
                )
                result = response.data.decode('UTF-8')
                self.assertEqual(result, 'non-Update')

                response = self.client.post(
                    '/file_walk?update=True', json=params
                )
                result = response.data.decode('UTF-8')
                self.assertEqual(result, 'Update')

        with patch('patroller.return_parsers') as mocked_call:
            mocked_call.return_value = ['MediaInfo']
            response = self.client.get(
                '/file_walk'
            )
            result = response.data.decode('UTF-8')
            self.assertEqual(result, '{"files":["MediaInfo"]}\n')


if __name__ == '__main__':
    unittest.main()
