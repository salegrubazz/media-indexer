from mongoengine import (
    Document,
    DynamicEmbeddedDocument,
    EmbeddedDocumentField,
    ListField,
    StringField,
)  # all imports needed for this database


# this is a set of primary data given by parser
class FileData(DynamicEmbeddedDocument):
    def __repr__(self):
        return str({field: self[field] for field in self._fields_ordered})


# this is a set of secondary data given by parser when parsing streams
class StreamData(DynamicEmbeddedDocument):
    def __repr__(self):
        return str({field: self[field] for field in self._fields_ordered})


# this is the object of this database
class File(Document):
    file_path = StringField(required=True, unique=True, max_length=1024)
    folder_path = StringField(required=True, unique=False, max_length=1024)
    data_key_list = ListField(StringField(
        required=False, unique=False, max_length=1024))
    file_data = EmbeddedDocumentField(FileData)
    stream_data = ListField(EmbeddedDocumentField(StreamData))

    def __repr__(self):
        return str({
            'file_path': self.file_path,
            'folder_path': self.folder_path,
            'data_key_list': repr(self.data_key_list),
            'file_data': repr(self.file_data),
            'stream_data': repr(self.stream_data),
        })

    def asdict(self):
        return {
            'file_path': self.file_path,
            'folder_path': self.folder_path,
            'data_key_list': self.data_key_list,
            'file_data': self.file_data,
            'stream_data': self.stream_data,
        }
