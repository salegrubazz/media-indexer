indexer_port = 'http://0.0.0.0:5000'  # port of indexer module
metagen_port = 'http://0.0.0.0:8000'  # port of metagen module
patroller_port = 'http://0.0.0.0:3000'  # port of patroller module
indexer_socket = 5000       # socket for indexer module
metagen_socket = 8000       # socket for metagen module
patroller_socket = 3000     # socket for patroller module
parsers = ['MediaInfo']  # list of all parsers
