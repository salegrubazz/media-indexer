import unittest
import os
import indexer
import config
import requests
import test_json
from models import File
from mongoengine import connect, disconnect
import ast

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
f_name = ROOT_DIR + '/test_files/test_file.txt'
folder_path = ROOT_DIR + '/test_files'


class TestPerson(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        disconnect()
        connect('mongoenginetest', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        data = test_json.test
        indexer.add_file(data)
        print('setUp')

    def tearDown(self):
        tmp = File.objects.get(file_path=f_name)
        tmp.delete()
        print('tearDown\n')

    def test_patrol(self):
        params = {'params': {'queryParser': {
            'chosenParser': 'MediaInfo'}, 'data': {'path': folder_path}},
        }

        response = requests.post(
            config.patroller_port+'/file_walk', json=params
        )
        result = ast.literal_eval(response.text)
        self.assertEqual(list(result.values())[0][0], f_name)

        response = requests.post(
            config.patroller_port+'/file_walk?update=True', json=params
        )
        result = ast.literal_eval(response.text)
        self.assertEqual(list(result.values())[0][0], 'Updated: '+f_name)

        response = requests.get(
            config.patroller_port+'/file_walk'
        )
        self.assertEqual(response.text, '{"files":["MediaInfo"]}\n')


if __name__ == '__main__':
    unittest.main()
