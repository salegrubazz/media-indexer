from flask import Flask, request		# flask framework and http methods
import requests  # json tools and python http methods
from pymediainfo import MediaInfo		# wrapper for mediaInfo
import config      # config module with all module ports


app = Flask(__name__)
parser_sequencer = False        # this is a global for limiting parser calls


# This is a function that calls mediainfo wrapper and sends data to indexer.py.
def call_mediainfo(target_fname):
    # calls MediaInfo through wrapper
    global parser_sequencer
    parser_sequencer = True
    info = MediaInfo.parse(filename=target_fname)
    parser_sequencer = False
    # TODO: error handling for mediaInfo (SEGFAULT)
    # TODO: /media/rtrk/500GB/2015-12-01/BMW Streams/DVB-T/HD Sender/
    # Lijon_M6HD_18_04_2013.ts -- This stream is making the segfault
    info_data = info.to_data()  # returns a JSON representation of the object
    # info sends meta-data to indexer.py
    response = requests.post(config.indexer_port, json=info_data)
    print('\nMediaInfo parsed file_path:'+target_fname)
    print('Post request status:')
    print(response.status_code)
    response.raise_for_status()
    return response.text


"""
This route receives a path to a file, which it uses as an argument
to extract meta-data with a parser of choice, choice being made with
an argument 'parser' in http request. If no argument is sent, MediaInfo
will be used as default.
MediaInfo is called through the pymediainfo wrapper. After that, this
function sends that data to indexer.py module through the post method.
"""
@app.route('/', methods=['POST'])
def extract_metadata():
    response = request.json  # extracts json argument of received data
    parser = response['parser']  # extracts parser that is going to be used
    target_fname = response['path']  # extracts path of file
    if parser == 'MediaInfo':
        # calls MediaInfo through wrapper
        while parser_sequencer is True:
            pass
        response = call_mediainfo(target_fname)
        return response
    else:
        # put elif here for any additional option of metadata indexer/parser
        return '\nERROR: Parser not implemented.\n'


if __name__ == '__main__':
    # 0.0.0.0 host is local IP in flask
    app.run(host='0.0.0.0', port=config.metagen_socket)
