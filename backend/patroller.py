# Patroller_API.py
from flask import Flask, request        # flask framework and http methods
from flask_cors import CORS     # additional http methods
import requests     # general http methods
import os       # for os.walk()
import config     # config module with all module ports and parser list
import time


app = Flask(__name__)
CORS(app)
request_sequencer = False


# This is a function that requests data from indexer.py module.
def request_indexer_data(f_path):
    data = requests.get(config.indexer_port, params=f_path)
    data = data.json()
    data = data['files']
    return data


# This is a function that returns a list of available parsers.
def return_parsers():
    return config.parsers


# This is a function that deletes obsolete folders from the database.
def obsolete_folder_delete(obsolete_folder_list):
    return_list = []
    for obsolete_folder in obsolete_folder_list:
        f_path = {'folder_path': obsolete_folder}
        for elem_file in request_indexer_data(f_path):
            # TODO: check needed for safety reasons
            response = requests.delete(config.indexer_port,
                                       json={'file_path': elem_file})
            return_list.append(response.text)
    return return_list


# This is a function that walks through data.
# It is the basic variant of the indexing functionality.
# It is called when admin checks 'don't update' button.
def file_walk(folder_path, parser):
    output_files = []
    global request_sequencer
    # this will walk through every file in root directory
    # it will also walk through every subfolder
    for root, dirs, files in os.walk(folder_path):
        for file_name in files:
            while request_sequencer is True:
                pass
            file_path = root + '/' + file_name
            # this post method calls metagen parser through use of arguments
            # currently it is set on MediaInfo
            # TODO: this is where parser if else should be implemented when--
            # additional parsers get implemented in metagen.

            # error handling system for segmentation fault thrown by parser.
            request_sequencer = True
            try:
                requests.post(config.metagen_port,
                              json={
                                  'path': file_path,
                                  'parser': parser
                              })
                # list of file_paths that went through indexing
                output_files.append(file_path)
                request_sequencer = False
            except requests.exceptions.ConnectionError:
                output_files.insert(0,
                                    'Memory error on parser while parsing object. Could not parse this file: '+file_path)
                time.sleep(3)  # Sleep until metagen has time to refresh
                request_sequencer = False

    return {
        'files': output_files
    }


# This is a function that walks through data.
# It is an advanced variant of the indexing functionality.
# It checks for any obsolete folders, subfolders and files.
# It is called on default from admin.
def file_walk_update(folder_path, parser):
    global request_sequencer
    output_files = []
    folder_list = []
    db_file_paths = {}
    current_file_paths = {}
    # this will also walk through folder_path folder and every subfolder
    for root, dirs, files in os.walk(folder_path):
        # list of folder_paths to be used for updating.
        folder_list.append(root)
        # dict of lists, key is folder_path and value are file_paths of all--
        # files in that folder. Populated with information from database--
        # through use of get function.
        # it is used for checking which files exist in DB but not on memory.
        db_file_paths[root] = request_indexer_data({'folder_path': root})
        # same as db_file_paths, but with folders and files in memory.
        current_file_paths[root] = [root+'/'+s for s in files]
        # this will walk through every file in root directory.
        for file_name in files:
            while request_sequencer is True:
                pass
            # creates full file path for current file.
            file_path = root + '/' + file_name
            # this post method calls metagen parser through use of arguments.
            # currently it is set on MediaInfo.
            # TODO: this is where parser if else should be implemented when--
            # additional parsers get implemented in metagen.
            # error handling system for segmentation fault thrown by parser.
            request_sequencer = True
            try:
                response = requests.post(config.metagen_port,
                                         json={
                                             'path': file_path,
                                             'parser': parser
                                         })
                output_files.append(response.text)
                request_sequencer = False
            except requests.exceptions.ConnectionError:
                output_files.insert(0,
                                    'Memory error on parser while parsing object. Could not parse this file: '+file_path)
                time.sleep(3)  # Sleep until metagen has time to refresh
                request_sequencer = False
    # top level folder path.
    current_folder_start = {'folder_map_path': folder_list[0]}
    # list containing top level folder path and all subfolder paths.
    db_folder_list = request_indexer_data(current_folder_start)
    # difference between current folder list and DB folder list.
    obsolete_folder_list = list(set(db_folder_list) - set(folder_list))
    # deletes all files in folders that do not exist on memory but are in DB.
    response = obsolete_folder_delete(obsolete_folder_list)
    for item in response:
        output_files.append(item)
    # deletes all files that do not exist on memory but are in DB.
    for folder_path in db_file_paths:
        # checks the difference between file_paths in DB and in folder
        dlt_list = list(set(db_file_paths[folder_path]) - set(
            current_file_paths[folder_path]))
        # if there is a difference, delete all different files from DB
        if dlt_list:
            for elem in dlt_list:
                response = requests.delete(config.indexer_port,
                                           json={'file_path': elem})
                output_files.append(response.text)

    return {
        'files': output_files
    }


"""
This route handles post requests from admin and
creates post requests that are sent to metagen.py
module. It has one argument in the post method,
update. When set to true, function file_walk_update
will be called. If not true, default file_walk will
be called.
It also handles get requests sent by admin. This
method returns a list of available parsers for
media indexing.
"""
@app.route('/file_walk', methods=['GET', 'POST'])
def patrol():
    # This method returns a list of parsers.
    if request.method == 'GET':
        parser_list = return_parsers()

        return {
            'files': parser_list
        }

    # This method is used for indexing.
    if request.method == 'POST':
        # checks if update argument is sent.
        update = request.args.get('update', None)
        response = request.json
        response = response['params']
        # this will handle the value from incoming json package.
        # this value represents folder path that should be root for indexing.
        folder_path = response['data']
        folder_path = folder_path['path']
        # this value represents parser that should be used for indexing.
        chosen_parser = response['queryParser']
        chosen_parser = chosen_parser['chosenParser']
        # if update = true is sent with post method from admin -
        # file_walk_update will be called.
        if update:
            return file_walk_update(folder_path, chosen_parser)
        else:
            return file_walk(folder_path, chosen_parser)


if __name__ == '__main__':
    # 0.0.0.0 host is local IP in flask
    app.run(host='0.0.0.0', port=config.patroller_socket)
