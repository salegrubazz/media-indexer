module.exports = {
  root: true,
  env: {
    node: true,
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard',
  ],
  rules: {
    'no-console': 0,
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'comma-dangle': [
      'error',
      'always-multiline',
    ],
    'eol-last': [ 'error', 'always' ],
    'no-multiple-empty-lines': ['error', { 'max': 2, 'maxEOF': 1 }],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
}
