# Gofer Media Indexer v1.0

What is this?
-------------
It is a media indexing web service that returns filepaths to searched for data on a certain server.

To understand what it's useful for, we can take the following situation:

You have a large database of media files. You need a list of files that have a certain specification in common (say you need all mp3 files that are precisely 3 minutes long). 
1. You can manualy search all files and make a list of all specified files. 
2. You can use Gofer to index your files and then search wished specifications in less then a second.

Option number 2 is obviously more convenient. This option is available through use of a media parsing program (example: MediaInfo) in combination with MongoDB. The parsing program is used for extraction of meta-data about indexed files. This data is then stored in MongoDB instance. After that, through use of a user-friendly interface created in Vue.js, the user can query data available in the database.


Important Note About Project Status & Development
-------------------------------------------------

**NOTE**: This is a student project. Project is based on an idea of a universal media indexer.

**REQUIREMENTS**: Linux Ubuntu 16.04 or 18.04

To set up the program, run the following commands on Linux:

* bash setup_gofer_on_ubuntu.sh

To run the program, run the following commands on Linux:

* bash run_gofer.sh

Unit tests are located in backend folder. While positioned in backend folder, run the following command on Linux to run them:

* bash test_gofer_backend.sh

To clear the database, while positioned in backend folder, run the following command:

* bash clear_metabase.sh 

Future extensions
----------------

This project already contains a plan for future extensions of the solution - see the [Missing_Features.rst](Missing_Features.rst) file for details.

License
----------------

This project is licensed under the BSD 3-clause "New" or "Revised" License - see the [LICENSE.txt](LICENSE.txt) file for details.


Acknowledgments
----------------

Gofer media indexer has originally been developed by Aleksandar Gruba.
Also, many thanks go to my technical mentor, Branimir Kovacevic, for helping out through the whole process.
